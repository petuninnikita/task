package com.petunin.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Set;

@Entity(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    @NotBlank
    @Size(min = 5, max = 255)
    private String name;

    @Column(name = "amount")
    @NotNull
    private BigDecimal amount;

    @Column(name = "is_active")
    @NotNull
    private Boolean isActive;

    @Column(name = "login")
    @NotBlank
    @Size(min = 3, max = 255)
    private String login;

    @Column(name = "password")
    @Size(max = 255)
    private String password;

    @OneToMany
    @JoinColumn(name = "customer_id")
    private Set<PartnerMapping> partnerMappings;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<PartnerMapping> getPartnerMappings() {
        return partnerMappings;
    }

    public void setPartnerMappings(Set<PartnerMapping> partnerMappings) {
        this.partnerMappings = partnerMappings;
    }
}
