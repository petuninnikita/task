package com.petunin.controller;

import com.petunin.model.PartnerMapping;
import com.petunin.service.PartnerMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
public class PartnerMappingController {
    @Autowired
    private PartnerMappingService partnerMappingService;

    @GetMapping("/partnerMappings")
    public Set<PartnerMapping> getPartnerMappings(@RequestAttribute(value = "id") Long id) {
        return partnerMappingService.getPartnerMappingsForCustomer(id);
    }

    @GetMapping("/partnerMappings/{id}")
    public PartnerMapping getPartnerMapping(@PathVariable Long id) {
        return partnerMappingService.getPartnerMapping(id);
    }

    @PutMapping("/partnerMappings/{id}")
    public void updatePartnerMapping(@PathVariable Long id,
                                     @RequestAttribute(value = "id") Long currentUserId,
                                     @RequestParam String identifier,
                                     @RequestParam String userId,
                                     @RequestParam String userName,
                                     @RequestParam String userAvatarUrl) {
        partnerMappingService.updatePartnerMapping(id, currentUserId, identifier, userId, userName, userAvatarUrl);
    }

    @DeleteMapping("/partnerMappings/{id}")
    public void deletePartnerMapping(@PathVariable Long id, @RequestAttribute(value = "id") Long userId) {
        partnerMappingService.deletePartnerMapping(id, userId);
    }

    @PostMapping("/partnerMappings")
    public PartnerMapping createPartnerMapping(@RequestAttribute(value = "id") Long id,
                                               @RequestParam String identifier,
                                               @RequestParam String userId,
                                               @RequestParam String userName,
                                               @RequestParam String userAvatarUrl) {
        return partnerMappingService.createPartnerMapping(id, identifier, userId, userName, userAvatarUrl);
    }
}
