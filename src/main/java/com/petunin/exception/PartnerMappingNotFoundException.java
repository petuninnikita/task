package com.petunin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class PartnerMappingNotFoundException extends RuntimeException {
    public PartnerMappingNotFoundException() {
        super("Partner mapping not found");
    }
}
