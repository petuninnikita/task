package com.petunin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class UnauthorizedModificationException extends RuntimeException {
    public UnauthorizedModificationException() {
        super("Unauthorized modification");
    }
}
