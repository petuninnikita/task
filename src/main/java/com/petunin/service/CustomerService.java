package com.petunin.service;

import com.petunin.exception.CustomerNotFoundException;
import com.petunin.model.Customer;
import com.petunin.model.repo.CustomerRepo;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepo customerRepo;

    public Customer getCustomer(Long id) {
        Optional<Customer> customer = customerRepo.findById(id);

        if (customer.isPresent()) {
            return customer.get();
        } else {
            throw new CustomerNotFoundException();
        }
    }

    public List<Customer> getCustomers() {
        return IterableUtils.toList(customerRepo.findAll());
    }
}
