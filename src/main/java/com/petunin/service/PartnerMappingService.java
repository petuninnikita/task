package com.petunin.service;

import com.petunin.exception.PartnerMappingNotFoundException;
import com.petunin.exception.UnauthorizedModificationException;
import com.petunin.model.PartnerMapping;
import com.petunin.model.repo.PartnerMappingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class PartnerMappingService {
    @Autowired
    private PartnerMappingRepo partnerMappingRepo;

    public Set<PartnerMapping> getPartnerMappingsForCustomer(Long id) {
        return partnerMappingRepo.findAllByCustomerId(id);
    }

    public PartnerMapping getPartnerMapping(Long id) {
        Optional<PartnerMapping> partnerMapping = partnerMappingRepo.findById(id);
        if (partnerMapping.isPresent()) {
            return partnerMapping.get();
        } else {
            throw new PartnerMappingNotFoundException();
        }
    }

    public void updatePartnerMapping(Long id,
                                     Long userId,
                                     String identifier,
                                     String partnerUserId,
                                     String userName,
                                     String userAvatarUrl) {
        Optional<PartnerMapping> partnerMappingOptional = partnerMappingRepo.findById(id);
        if (partnerMappingOptional.isPresent()) {
            PartnerMapping partnerMapping = partnerMappingOptional.get();

            checkModificationAccess(partnerMapping, userId);

            partnerMapping.setIdentifier(identifier);
            partnerMapping.setUserAvatarUrl(userAvatarUrl);
            partnerMapping.setUserId(partnerUserId);
            partnerMapping.setUserName(userName);
            partnerMapping.setCustomerId(id);

            partnerMappingRepo.save(partnerMapping);
        } else {
            throw new PartnerMappingNotFoundException();
        }
    }

    public PartnerMapping createPartnerMapping(Long id,
                                               String identifier,
                                               String userId,
                                               String userName,
                                               String userAvatarUrl) {
        PartnerMapping partnerMapping = new PartnerMapping();
        partnerMapping.setIdentifier(identifier);
        partnerMapping.setUserAvatarUrl(userAvatarUrl);
        partnerMapping.setUserId(userId);
        partnerMapping.setUserName(userName);
        partnerMapping.setCustomerId(id);

        return partnerMappingRepo.save(partnerMapping);
    }

    public void deletePartnerMapping(Long id, Long userId) {
        Optional<PartnerMapping> partnerMappingOptional = partnerMappingRepo.findById(id);
        if (partnerMappingOptional.isPresent()) {
            PartnerMapping partnerMapping = partnerMappingOptional.get();

            checkModificationAccess(partnerMapping, userId);

            partnerMappingRepo.delete(partnerMapping);
        } else {
            throw new PartnerMappingNotFoundException();
        }
    }

    private void checkModificationAccess(PartnerMapping partnerMapping, Long userId) {
        if (!partnerMapping.getCustomerId().equals(userId)) {
            throw new UnauthorizedModificationException();
        }
    }
}
